#pragma once


class Point{
public:
    int x, y;
    Point(){};
    Point(int x, int y): x(x), y(y){};
    Point(const Point& pt): x(pt.x), y(pt.y){};

    Point operator=(Point pt);
    Point operator*(int n);
    Point operator/(int n);
    Point operator+(Point pt);
    Point operator-(Point pt);
    bool operator==(Point pt);
    void setCoordinetes(int x, int y){
        *this = Point(x, y);
    }
};