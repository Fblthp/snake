#include "Snake.h"

Snake::Snake(int l, int hx, int hy): length(l){
    for(int i =0; i < l; i++){
        this->body.push_back(new snakeElement(hx - i, hy));
    }
    this->direction = Point(1, 0);
    init_pair(SNAKE_COLOR, 9, 9);

}

void Snake::changeDirection(int x, int y){
    if(this->direction.x != x && this->direction.y != y){
        this->direction.setCoordinetes(x, y);
    }

}

void Snake::move(){
    Point prev = this->body[0]->coordinates + this->direction;
    Point temp;

    for(int i = 0; i < this->length; i++){
        temp = this->body[i]->coordinates;
        this->body[i]->coordinates = prev;
        prev = temp;
    }
}

void Snake::printSnake(WINDOW *win){
    wattron(win, COLOR_PAIR(SNAKE_COLOR));
    for(auto snakePart: this->body){
        mvwprintw(win, snakePart->coordinates.y, snakePart->coordinates.x, "%c", '*');

    }
}