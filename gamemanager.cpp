#include <cstdlib>
#include <iostream>
#include "gamemanager.h"

GameManager::GameManager(const char *mapName)
{
    this->map = Map(mapName);
}


void findFreeSpace(int* x, int* y, GameManager* gm){
    while (TRUE) {
        *x = rand() % gm->map.rows;
        *y = rand() % gm->map.cols;
        bool snake = FALSE;

        for(auto snakeEl: gm->snake.body){
            if(snakeEl->coordinates.x == *x and snakeEl->coordinates.x == *x){
                snake = TRUE;
                break;
            }
        }
        if (gm->map.map[*y][*x] == '.' and not snake){
            return;
        }
    }
}

int GameManager::keyCallback(int key)
{
    switch(key)
    {
        case KEY_UP: this->snake.changeDirection(0, -1);
            break;
        case KEY_DOWN: this->snake.changeDirection(0, 1);
            break;
        case KEY_RIGHT: this->snake.changeDirection(1, 0);
            break;
        case KEY_LEFT: this->snake.changeDirection(-1, 0);
            break;
    }
    return 0;
}

void GameManager::initConsole()
{
    initscr();
    clear();
    start_color();
    noecho();
    srand(time(0));

    this->Window = newwin(30, 60, 0, 0);
    this->Info = newwin(30, 20, 0, 61);
    keypad(this->Window, TRUE);
}

void GameManager::startGame(){
    this->initConsole();
    this->Score = -1;
    this->snake = Snake(3, 3, 3);
    this->refreshGrid();
    this->snake.printSnake(this->Window);
    this->spawnFood();
    wrefresh(this->Window);

    this->gameLoop();
}

void GameManager::gameLoop() {
//    this->startGame();

    while (TRUE) {
        halfdelay(1.5);
        int command = wgetch(this->Window);
        this->keyCallback(command);
        this->checkTarget();
        this->refreshGrid();
        this->snake.move();
        this->snake.printSnake(this->Window);
        this->printFood();
        this->refreshInfo();
        wrefresh(this->Window);
    }
}

void GameManager::checkTarget(){
    Point target(this->snake.body[0]->coordinates + this->snake.direction);
    if(not this->map.rangeCheck(target.x, target.y) or (this->map.map[target.x][target.y]) == '#'){
        this->endGame();
    }
    for(auto snakeEl: this->snake.body){
        if(snakeEl->coordinates == target){
            this->endGame();
        }
    }
    if(this->food == target){
        this->spawnFood();
    }
}

void GameManager::printFood(){
    init_pair(4, 10, 10);
    wattron(this->Window, COLOR_PAIR(4));
    mvwprintw(this->Window, this->food.y, this->food.x, "%c", '*');
}

void GameManager::endGame(){
    mvwprintw(this->Info, 8, 0, "%s", "You Lost!");
    mvwprintw(this->Info, 9, 0, "Final Score: %d", this->Score);

    wrefresh(this->Info);
    while (TRUE){
        int command = wgetch(this->Window);
        if(command != -1){
            this->startGame();
        }
    }
}

void GameManager::spawnFood(){
    findFreeSpace(&this->food.x, &this->food.y, this);
//    this->snake.addElement();
    this->snake.body.push_back(new snakeElement(0, 0));
    this->snake.length++;
    this->Score++;
}

void GameManager::refreshGrid() {
    this->map.printMap(this->Window);
}

void GameManager::refreshInfo()
{
    wclear(this->Info);
    mvwprintw(this->Info, 0, 0, "%s", "Game info:");
    mvwprintw(this->Info, 2, 0, "Food position: %d %d", this->food.x, this->food.y);
    mvwprintw(this->Info, 4, 0, "Current Score: %d", this->Score);



    wrefresh(this->Info);
}
