#include "Point.h"

bool Point::operator==(Point pt){
    return (this->x == pt.x && this->y == pt.y);
}

Point Point::operator*(int n){
    return Point(this->x * n, this->y * n);
}

Point Point::operator/(int n){
    return Point(this->x / n, this->y / n);
}

Point Point::operator=(Point pt){
    this->x = pt.x;
    this->y = pt.y;
};

Point Point::operator+(Point pt){
    return Point(this->x + pt.x, this->y + pt.y);
};

Point Point::operator-(Point pt){
    return Point(this->x - pt.x, this->y - pt.y);
};