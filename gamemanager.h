#pragma once
#include <ncurses.h>
#include <panel.h>
#include <vector>
#include "map.h"
#include "Snake.h"


class GameManager
{
public:
    WINDOW *Window, *Info;
    Map map;
    Snake snake;
    Point food;
    int Score;
    GameManager(const char *mapName);
    void spawnFood();
    void refreshGrid();
    void refreshInfo();
    int keyCallback(int key);
    void checkTarget();
    void initConsole();
    void startGame();
    void gameLoop();
    void endGame();
    void printFood();
};