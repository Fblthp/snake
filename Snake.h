#pragma once

#include <deque>
#include <vector>
#include <ncurses.h>
#include "Point.h"

using namespace std;


class snakeElement{
public:
    Point coordinates;
    snakeElement(int x, int y): coordinates(x, y){};
};

class Snake{
public:
    int SNAKE_COLOR = 3;
    int length;
    deque<snakeElement*> body;
    Point direction;
    Snake() : length(0){};
    Snake(int l, int hx, int hy);
    void changeDirection(int x, int y);
    void move();
    void printSnake(WINDOW *win);
};

