#include "map.h"
#include <fstream>
#include <iostream>


Map::Map(const char* name_map){
    const int WALL_COLOR = 1;
    const int BACKGROUND_COLOR = 2;

    std::ifstream input(name_map);
    int r, c;
    input >> r;
    input >> c;
    this->rows = r;
    this->cols = c;
    this->map.resize(rows);

    for (int i = 0; i < this->rows; i++)
    {
        this->map[i].resize(cols);
        for (int j = 0; j < this->cols; j++)
        {
            input >> this->map[i][j];
        }
    }
}

void Map::printMap(WINDOW *win) {
    init_pair(1, 7, 7);
    init_pair(2, 5, 5);

    for (int i = 0; i < this->rows; i++) {
        for (int j = 0; j < this->cols; j++) {
            if (this->map[i][j] == '.'){
                wattron(win, COLOR_PAIR(1));
            }
            else {
                wattron(win, COLOR_PAIR(2));
            }
            mvwprintw(win, j, i, "%c", this->map[i][j]);
        }
        wrefresh(win);
    }
}


bool Map::rangeCheck(int x, int y){
    if (y < this->cols and
        x < this->rows and
        y >= 0 and x >= 0){
        return TRUE;
    }
    return FALSE;
}