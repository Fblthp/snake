#pragma once
#include <vector>
#include <ncurses.h>

using namespace std;

class Map
{
public:
    int cols, rows;
    vector <vector <char>> map;
    Map() {};
    Map(const char *name_map);
    void printMap(WINDOW *win);
    bool rangeCheck(int x, int y);

};